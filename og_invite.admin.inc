<?php
function invite_admin($form, &$form_state) {
  $form = array();

  $form['invite_validity'] = array(
    '#type' => 'textfield',
    '#title' => t('Validity period (in days)'),
    '#description' => '0 means that the invitations will never expire',
    '#default_value' => variable_get('invite_validity', 7),
    '#size' => 5,
    '#maxlength' => 5,
    '#required' => TRUE,
  );

  return system_settings_form($form);
}