<?php
function invite_form($form, &$form_state, $entity, $etid) {
  global $user;
  $form = array();

  $form['emails'] = array(
    '#title' => t('Email addresses'),
    '#type' => 'textarea',
    '#required' => TRUE,
    '#description' => t('Please enter one email address per line'),
  );

  if(!og_user_access_by_entity('invite without approval', $entity, $etid)) {
    $form['reason'] = array(
      '#type' => 'textarea',
      '#value' => 'Approval request',
      '#description' => t('Your invitations need to be approved by the group manager. Please describe in a few words the reasons for inviting these people.'),
    );
  }
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Invite friends',
    '#submit' => array('invite_send', $entity, $etid),
  );

  return $form;
}

function invite_page($hash) {

}